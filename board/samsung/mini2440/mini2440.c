/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002, 2010
 * David Mueller, ELSOFT AG, <d.mueller@elsoft.ch>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <dm.h>
#include <netdev.h>
#include <asm/io.h>
#include <asm/arch/s3c24x0_cpu.h>
#include <asm/mach-types.h>
#include <cpu_func.h>

DECLARE_GLOBAL_DATA_PTR;

#if defined(CONFIG_SHOW_BOOT_PROGRESS)
void show_boot_progress(int val)
{
	struct s3c24x0_gpio * const gpio = s3c24x0_get_base_gpio();
	writel(~(val << 5) & 0x1e0, &gpio->gpbdat);
}
#endif

/*
 * Miscellaneous platform dependent initialisations
 */

int board_early_init_f(void)
{
	struct s3c24x0_gpio * const gpio = s3c24x0_get_base_gpio();

	/* set up the I/O ports */
#if defined(CONFIG_SHOW_BOOT_PROGRESS)
	writel(0x00055455, &gpio->gpbcon);
	writel(0x000001E0, &gpio->gpbup);

#else
	writel(0x00044555, &gpio->gpbcon);
	writel(0x000007FF, &gpio->gpbup);
#endif

	writel(0xA02AA800, &gpio->gpecon);			/* SDI: GPE5-10 */
												/* GPE14=SCL, GPE15=SDA */
	writel(0x00000000, &gpio->gpgcon);        /* GPG8 Card Detect */

	writel(0x002AFAAA, &gpio->gphcon);        /* GPH3=RXD0, GPH2=TXD0 */
	writel(0x000007FF, &gpio->gphup);

	return 0;
}

void enable_caches(void)
{
	icache_enable();
	dcache_enable();
}

int board_init(void)
{
	/* arch number of MINI2440-Board */
	gd->bd->bi_arch_number = MACH_TYPE_MINI2440;

	/* adress of boot parameters */
	gd->bd->bi_boot_params = gd->ram_base + 0x100;

	icache_enable();
	dcache_enable();

	return 0;
}

int dram_init(void)
{
	/* dram_init must store complete ramsize in gd->ram_size */
	gd->ram_size = PHYS_SDRAM_1_SIZE;
	return 0;
}

#ifdef CONFIG_DRIVER_DM9000
int board_eth_init(struct bd_info *bis)
{
	return dm9000_initialize(bis);
}
#endif

#if defined(CONFIG_MMC) && !defined(CONFIG_SPL_BUILD)
static int mini2440_mmc_cd(struct mmc *card)
{
	struct s3c24x0_gpio * const gpio = s3c24x0_get_base_gpio();
	return !(readl(&gpio->gpgdat) & (0x100));
}

int board_mmc_getcd(struct mmc *card)
{
	return mini2440_mmc_cd(card);
}

int board_mmc_init(struct bd_info *bis)
{
	return s3cmmc_initialize(bis, mini2440_mmc_cd, NULL);
}

#if !CONFIG_IS_ENABLED(OF_CONTROL)
static const struct s3c24x0_mmc_plat s3c2440_mmc_platdata = {
	.getcd= mini2440_mmc_cd,
};

U_BOOT_DEVICE(s3c2440_mmc) = {
	.name = "s3cmmc",
	.platdata = &s3c2440_mmc_platdata,
};

#endif
#endif /* CONFIG_MMC */
