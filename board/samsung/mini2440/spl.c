#include <common.h>
#include <init.h>
#include <spl.h>

u32 spl_boot_device(void)
{
	return BOOT_DEVICE_RAM;
}

void board_init_f(ulong dummy)
{
	board_early_init_f();

#ifdef CONFIG_SPL_SERIAL_SUPPORT
	preloader_console_init();
#endif

	dram_init();
}
