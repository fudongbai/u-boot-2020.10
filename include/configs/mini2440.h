/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <garyj@denx.de>
 * David Mueller <d.mueller@elsoft.ch>
 *
 * Configuation settings for the SAMSUNG SMDK2410 board.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/*
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_SYS_ARM_CACHE_WRITETHROUGH

/* input clock of PLL (the MINI2440 has 12MHz input clock) */
#define CONFIG_SYS_CLK_FREQ	12000000

#if defined(CONFIG_USE_BOOTARGS)
#define CONFIG_CMDLINE_TAG	/* enable passing of ATAGs */
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_INITRD_TAG
#endif

#define CONFIG_BOOTFILE		"uImage"

/*
 * Miscellaneous configurable options
 */
#define CONFIG_SYS_CBSIZE	256
/* Print Buffer Size */
#define CONFIG_SYS_PBSIZE	(CONFIG_SYS_CBSIZE + \
				sizeof(CONFIG_SYS_PROMPT)+16)
#define CONFIG_SYS_MAXARGS	16
#define CONFIG_SYS_BARGSIZE	CONFIG_SYS_CBSIZE

#define CONFIG_SYS_MEMTEST_START	0x30000000	/* memtest works on */
#define CONFIG_SYS_MEMTEST_END		0x33F00000	/* 63 MB in DRAM */

#define CONFIG_SYS_LOAD_ADDR		0x30800000

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define PHYS_SDRAM_1		0x30000000 /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE	0x04000000 /* 64 MB */

/*
 * Size of malloc() pool
 * BZIP2 / LZO / LZMA need a lot of RAM
 */
#define CONFIG_SYS_MALLOC_LEN	(4 * 1024 * 1024)

#define CONFIG_SYS_MONITOR_LEN	(448 * 1024)
#define CONFIG_SYS_MONITOR_BASE	CONFIG_SYS_FLASH_BASE

/* additions for new relocation code, must be added to all boards */
#define CONFIG_SYS_SDRAM_BASE	PHYS_SDRAM_1
#define CONFIG_SYS_INIT_SP_ADDR	(CONFIG_SYS_SDRAM_BASE + 0x1000 - \
				GENERATED_GBL_DATA_SIZE)


#ifdef CONFIG_NET
# define CONFIG_IPADDR    192.168.0.89
# define CONFIG_ETHADDR   0c:96:e6:15:09:12
# define CONFIG_HOSTNAME  "mini2440"
# define CONFIG_SERVERIP  192.168.0.3
# define CONFIG_GATEWAYIP 192.168.0.1
# define CONFIG_NETMASK   255.255.255.0
#endif /* CONFIG_NET */

#define	CONFIG_EXTRA_ENV_SETTINGS						\
	"ethaddr=" __stringify(CONFIG_ETHADDR) "\0"			\
	"ramdisk=ramdisk.cpio.img.uboot\0"					\
	"kernel_addr=0x31000000\0"							\
	"ramdisk_addr=0x33000000\0"							\
	"mmcboot="											\
		"mmc read $kernel_addr 0x500 0x3000; "			\
		"mmc read $ramdisk_addr 0x5000 0x2000; "		\
		"bootm $kernel_addr $ramdisk_addr\0"			\
	"tftpboot="											\
		"tftpboot $kernel_addr $bootfile; "				\
		"tftpboot $ramdisk_addr $ramdisk; "				\
		"bootm $kernel_addr $ramdisk_addr\0"

#define CONFIG_BOOTCOMMAND		"run mmcboot"

/* Hardware drivers */
/* DM9000 */
#define	CONFIG_DRIVER_DM9000		1

#ifdef CONFIG_DRIVER_DM9000
# define	CONFIG_DM9000_BASE		0x20000000
# define	DM9000_IO			CONFIG_DM9000_BASE
# define	DM9000_DATA			(CONFIG_DM9000_BASE + 4)    //TODO
# define	CONFIG_DM9000_USE_16BIT		1
# define	CONFIG_DM9000_NO_SROM		1
# define	CONFIG_NET_RETRY_COUNT		20
# undef	CONFIG_DM9000_DEBUG
#endif /* CONFIG_DRIVER_DM9000 */

#define CONFIG_SYS_NAND_BASE		0x4E000000
#define CONFIG_SYS_MAX_NAND_DEVICE	1

# endif /* __CONFIG_H */
