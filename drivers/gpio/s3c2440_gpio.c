/*
 * Copyright (C) 2012
 * Gabriel Huau <contact@huau-gabriel.fr>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
#include <common.h>
#include <dm.h>
#include <asm/arch/s3c2440.h>
#include <asm/gpio.h>
#include <asm/io.h>
#include <errno.h>

#define GPIO_INPUT  0x0
#define GPIO_OUTPUT 0x1

#define S3C_GPIO_CON	0x0
#define S3C_GPIO_DAT	0x4

struct s3c_gpio_platdata {
	void *base;
	int gpio_count;
	const char *bank_name;
};

static int s3c_gpio_set_value(struct udevice *dev, unsigned gpio, int value)
{
	struct s3c_gpio_platdata *plat = dev_get_platdata(dev);

	if (value)
		setbits_le32(plat->base + S3C_GPIO_DAT, 1 << (gpio & 0xf));
	else
		clrbits_le32(plat->base + S3C_GPIO_DAT, 1 << (gpio & 0xf));

	return 0;
}

static int s3c_gpio_get_value(struct udevice *dev, unsigned gpio)
{
	struct s3c_gpio_platdata *plat = dev_get_platdata(dev);

	return !!(readl(plat->base + S3C_GPIO_DAT) & (1 << (gpio & 0xf)));
}

static int s3c_gpio_direction(void *addr, unsigned gpio, uint8_t dir)
{
	const uint32_t mask = 0x3 << ((gpio & 0xf) << 1);
	const uint32_t dirm = dir << ((gpio & 0xf) << 1);

	clrsetbits_le32(addr, mask, dirm);
	return 0;
}

static int s3c_gpio_direction_input(struct udevice *dev, unsigned gpio)
{
	struct s3c_gpio_platdata *plat = dev_get_platdata(dev);

	return s3c_gpio_direction(plat->base, gpio, GPIO_INPUT);
}

static int s3c_gpio_direction_output(struct udevice *dev, unsigned gpio,
				       int value)
{
	struct s3c_gpio_platdata *plat = dev_get_platdata(dev);
	s3c_gpio_direction(plat->base, gpio, GPIO_OUTPUT);
	return s3c_gpio_set_value(dev, gpio, value);
}

static int s3c_gpio_get_function(struct udevice *dev, unsigned gpio)
{
	struct s3c_gpio_platdata *plat = dev_get_platdata(dev);
	const uint32_t mask = 0x3 << ((gpio & 0xf) << 1);
	uint32_t val, func;

	val = readl(plat->base) & mask;
	func = val >> ((gpio & 0xf) << 1);

	if (func == GPIO_OUTPUT)
		return GPIOF_OUTPUT;
	else if (func == GPIO_INPUT)
		return GPIOF_INPUT;
	else
		return GPIOF_FUNC;
}

static const struct dm_gpio_ops s3c_gpio_ops = {
	.direction_input	= s3c_gpio_direction_input,
	.direction_output	= s3c_gpio_direction_output,
	.get_value		= s3c_gpio_get_value,
	.set_value		= s3c_gpio_set_value,
	.get_function		= s3c_gpio_get_function,
};

static int s3c_gpio_ofdata_to_platdata(struct udevice *dev)
{
	struct s3c_gpio_platdata *plat = dev_get_platdata(dev);
	fdt_addr_t addr = dev_read_addr(dev);

	if (addr == FDT_ADDR_T_NONE)
		return -EINVAL;

	plat->base = (void *)addr;

	plat->gpio_count = dev_read_s32_default(dev, "s3c2440,gpio-bank-width", -1);
	plat->bank_name = dev_read_string(dev, "gpio-bank-name");

	return 0;
}

static int s3c_gpio_probe(struct udevice *dev)
{
	struct s3c_gpio_platdata *platdata = dev_get_platdata(dev);
	struct gpio_dev_priv *uc_priv = dev_get_uclass_priv(dev);

	uc_priv->gpio_count = platdata->gpio_count;
	uc_priv->bank_name = platdata->bank_name;

	return 0;
}

static const struct udevice_id s3c_gpio_ids[] = {
	{ .compatible = "samsung,s3c2440-gpio" },
	{ }
};

U_BOOT_DRIVER(s3c_gpio) = {
	.name	= "s3c_gpio",
	.id	= UCLASS_GPIO,
	.ops	= &s3c_gpio_ops,
	.ofdata_to_platdata = s3c_gpio_ofdata_to_platdata,
	.platdata_auto_alloc_size = sizeof(struct s3c_gpio_platdata),
	.of_match = s3c_gpio_ids,
	.probe	= s3c_gpio_probe,
};
