/*
 * (C) Copyright 2002
 * Gary Jennejohn, DENX Software Engineering, <garyj@denx.de>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <dm.h>
#include <linux/compiler.h>
#include <asm/arch/s3c24x0_cpu.h>

DECLARE_GLOBAL_DATA_PTR;

#if CONFIG_CONS_INDEX == 0
#define UART_NR	S3C24X0_UART0

#elif CONFIG_CONS_INDEX == 1
#define UART_NR	S3C24X0_UART1

#elif CONFIG_CONS_INDEX == 2
#define UART_NR	S3C24X0_UART2

#else
#error "Bad: you didn't configure serial ..."
#endif

/* Bit definitions of UTRSTAT register */
#define UTRSTAT_RXBUF (1 << 0)
#define UTRSTAT_TXBUF (1 << 1)

#include <asm/io.h>
#include <serial.h>

static void _serial_setbrg(const int dev_index, int baudrate)
{
	struct s3c24x0_uart *uart = s3c24x0_get_base_uart(dev_index);
	unsigned int reg = 0;
	int i;

	/* value is calculated so : (int)(PCLK/16./baudrate) -1 */
	reg = get_PCLK() / (16 * baudrate) - 1;

	writel(reg, &uart->ubrdiv);
	for (i = 0; i < 100; i++)
		/* Delay */ ;
}


/* Initialise the serial port. The settings are always 8 data bits, no parity,
 * 1 stop bit, no start bits.
 */
static int serial_init_dev(const int dev_index, int baudrate)
{
	struct s3c24x0_uart *uart = s3c24x0_get_base_uart(dev_index);

	/* FIFO enable, Tx/Rx FIFO clear */
	writel(0x07, &uart->ufcon);
	writel(0x0, &uart->umcon);

	/* Normal,No parity,1 stop,8 bit */
	writel(0x3, &uart->ulcon);
	/*
	 * tx=level,rx=edge,disable timeout int.,enable rx error int.,
	 * normal,interrupt or polling
	 */
	writel(0x245, &uart->ucon);

	_serial_setbrg(dev_index, baudrate);

	return (0);
}

/*
 * Read a single byte from the serial port. Returns 1 on success, 0
 * otherwise. When the function is succesfull, the character read is
 * written into its argument c.
 */
static int _serial_getc(const int dev_index)
{
	struct s3c24x0_uart *uart = s3c24x0_get_base_uart(dev_index);

	while (!(readl(&uart->utrstat) & 0x1))
		/* wait for character to arrive */ ;

	return readb(&uart->urxh) & 0xff;
}

/*
 * Output a single byte to the serial port.
 */
static void _serial_putc(const char c, const int dev_index)
{
	struct s3c24x0_uart *uart = s3c24x0_get_base_uart(dev_index);

	/* If \n, also do \r */
	if (c == '\n')
		serial_putc('\r');

	while (!(readl(&uart->utrstat) & 0x2))
		/* wait for room in the tx FIFO */ ;

	writeb(c, &uart->utxh);
}

#ifndef CONFIG_DM_SERIAL
/* Multi serial device functions */
#define DECLARE_S3C_SERIAL_FUNCTIONS(port) \
	int s3serial##port##_init(void) \
	{ \
		return serial_init_dev(port, gd->baudrate); \
	} \
	void s3serial##port##_setbrg(void) \
	{ \
		serial_setbrg_dev(port, gd->baudrate); \
	} \
	int s3serial##port##_getc(void) \
	{ \
		return serial_getc_dev(port); \
	} \
	int s3serial##port##_tstc(void) \
	{ \
		return serial_tstc_dev(port); \
	} \
	void s3serial##port##_putc(const char c) \
	{ \
		serial_putc_dev(port, c); \
	} \
	void s3serial##port##_puts(const char *s) \
	{ \
		serial_puts_dev(port, s); \
	}

#define INIT_S3C_SERIAL_STRUCTURE(port, __name) {	\
	.name	= __name,				\
	.start	= s3serial##port##_init,		\
	.stop	= NULL,					\
	.setbrg	= s3serial##port##_setbrg,		\
	.getc	= s3serial##port##_getc,		\
	.tstc	= s3serial##port##_tstc,		\
	.putc	= s3serial##port##_putc,		\
	.puts	= s3serial##port##_puts,		\
}

static inline void serial_setbrg_dev(unsigned int dev_index, int baudrate)
{
	_serial_setbrg(dev_index, baudrate);
}

static inline int serial_getc_dev(unsigned int dev_index)
{
	return _serial_getc(dev_index);
}

static inline void serial_putc_dev(unsigned int dev_index, const char c)
{
	_serial_putc(c, dev_index);
}

/*
 * Test whether a character is in the RX buffer
 */
static int _serial_tstc(const int dev_index)
{
	struct s3c24x0_uart *uart = s3c24x0_get_base_uart(dev_index);

	return readl(&uart->utrstat) & 0x1;
}

static inline int serial_tstc_dev(unsigned int dev_index)
{
	return _serial_tstc(dev_index);
}

static void _serial_puts(const char *s, const int dev_index)
{
	while (*s) {
		_serial_putc(*s++, dev_index);
	}
}

static inline void serial_puts_dev(int dev_index, const char *s)
{
	_serial_puts(s, dev_index);
}

DECLARE_S3C_SERIAL_FUNCTIONS(0);
struct serial_device s3c24xx_serial0_device =
INIT_S3C_SERIAL_STRUCTURE(0, "s3ser0");
DECLARE_S3C_SERIAL_FUNCTIONS(1);
struct serial_device s3c24xx_serial1_device =
INIT_S3C_SERIAL_STRUCTURE(1, "s3ser1");
DECLARE_S3C_SERIAL_FUNCTIONS(2);
struct serial_device s3c24xx_serial2_device =
INIT_S3C_SERIAL_STRUCTURE(2, "s3ser2");

__weak struct serial_device *default_serial_console(void)
{
#if CONFIG_CONS_INDEX == 0
	return &s3c24xx_serial0_device;
#elif CONFIG_CONS_INDEX == 1
	return &s3c24xx_serial1_device;
#elif CONFIG_CONS_INDEX == 2
	return &s3c24xx_serial2_device;
#endif
}

void s3c24xx_serial_initialize(void)
{
	serial_register(&s3c24xx_serial0_device);
	serial_register(&s3c24xx_serial1_device);
	serial_register(&s3c24xx_serial2_device);
}

#else

struct s3c24xx_serial_priv {
	int uart_index;
};

int s3c24xx_serial_setbrg(struct udevice *dev, int baudrate)
{
	struct s3c24xx_serial_priv *priv = dev_get_priv(dev);
	int idx = priv->uart_index;

	_serial_setbrg(idx, baudrate);

	return 0;
}

static int s3c24xx_serial_getc(struct udevice *dev)
{
	struct s3c24xx_serial_priv *priv = dev_get_priv(dev);
	int idx = priv->uart_index;

	return _serial_getc(idx);
}

static int s3c24xx_serial_putc(struct udevice *dev, const char ch)
{
	struct s3c24xx_serial_priv *priv = dev_get_priv(dev);
	int idx = priv->uart_index;

	_serial_putc(ch, idx);

	return 0;
}

static int s3c24xx_serial_pending(struct udevice *dev, bool input)
{
	struct s3c24xx_serial_priv *priv = dev_get_priv(dev);
	struct s3c24x0_uart *uart;
	int idx = priv->uart_index;
	uint32_t status;

	uart = s3c24x0_get_base_uart(idx);
	status = readl(&uart->utrstat);

	if (input)
		return status & UTRSTAT_RXBUF ? 1 : 0;
	else
		return status & UTRSTAT_TXBUF ? 0 : 1;
}

static const struct dm_serial_ops s3c24xx_serial_ops = {
	.setbrg = s3c24xx_serial_setbrg,
	.putc = s3c24xx_serial_putc,
	.getc = s3c24xx_serial_getc,
	.pending	= s3c24xx_serial_pending,
};

static int s3c24xx_serial_probe(struct udevice *dev)
{
	struct s3c24xx_serial_priv *priv = dev_get_priv(dev);
	int idx;

	idx = priv->uart_index = CONFIG_CONS_INDEX;
	serial_init_dev(idx, gd->baudrate);

	return 0;
}

#if CONFIG_IS_ENABLED(OF_CONTROL)
static const struct udevice_id s3c24xx_serial_ids[] = {
	{
		.compatible = "samsung,s3c2440-uart",
	},
	{ }
};
#endif

U_BOOT_DRIVER(serial_s3c24x0) = {
	.name	= "serial_s3c24x0",
	.id	= UCLASS_SERIAL,
#if CONFIG_IS_ENABLED(OF_CONTROL)
	.of_match = s3c24xx_serial_ids,
#endif
	.probe = s3c24xx_serial_probe,
	.ops	= &s3c24xx_serial_ops,
#if !CONFIG_IS_ENABLED(OF_CONTROL)
	.flags = DM_FLAG_PRE_RELOC,
#endif
	.priv_auto_alloc_size	= sizeof(struct s3c24xx_serial_priv),
};
#endif
